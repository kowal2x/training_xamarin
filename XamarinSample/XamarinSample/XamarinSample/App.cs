﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using Xamarin.Forms;
using XamarinSample.Page;
using XLabs.Forms.Mvvm;
using XLabs.Forms.Services;
using XLabs.Ioc;
using XLabs.Ioc.Ninject;
using XLabs.Platform.Services;

namespace XamarinSample
{
	public class App : Application
	{
		public App()
		{
			var kernel = new StandardKernel();
			var ninjectResolver = new NinjectResolver(kernel);
			Resolver.SetResolver(ninjectResolver);

			ViewFactory.Register<WelcomePage, WelcomePageViewModel>();
			ViewFactory.Register<SomePage, SomePageViewModel>();

			var np = new NavigationPage();
			MainPage = np;
			kernel.Bind<INavigationService>().ToMethod(t => new NavigationService(MainPage.Navigation));
			TimeNotifier.Start();

			Resolver.Resolve<INavigationService>().NavigateTo<WelcomePageViewModel>();
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
