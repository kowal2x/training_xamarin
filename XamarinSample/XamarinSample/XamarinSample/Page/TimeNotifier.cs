﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamarinSample.Page
{
	public static class TimeNotifier
	{
		static readonly ManualResetEvent _mre = new ManualResetEvent(false);
		static private CancellationTokenSource _cts;

		public static void Start()
		{
			_mre.Reset();
			_cts = new CancellationTokenSource();
			Task.Run(async () =>
			{
				while (true)
				{
					MessagingCenter.Send(new TimerEvent(DateTime.Now), string.Empty);
					await Task.Delay(TimeSpan.FromSeconds(1), _cts.Token);
				}
			}).ConfigureAwait(false);
		}
		public static void Stop()
		{
			_cts.Cancel();
			_mre.WaitOne();
		}
	}
}