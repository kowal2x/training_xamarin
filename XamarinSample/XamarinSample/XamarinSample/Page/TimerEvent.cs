﻿using System;

namespace XamarinSample.Page
{
	public class TimerEvent
	{
		public DateTime Now { get; set; }

		public TimerEvent(DateTime now)
		{
			Now = now;
		}
	}
}