using XLabs.Forms.Mvvm;

namespace XamarinSample.Page
{
	public class SomePageViewModel : ViewModel
	{
		public SomePageViewModel(string data = "")
		{
			Info = data;
		}

		public string Info { get; private set; }
	}
}