﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using XLabs.Forms.Mvvm;

namespace XamarinSample.Page
{
	public class WelcomePageViewModel : ViewModel
	{
		private Person _welcomePerson;
		private string _currentTime;

		public WelcomePageViewModel()
		{
			MessagingCenter.Subscribe<TimerEvent>(this, string.Empty, TimerEventAction);

			DoSomethingCommand = new Command(DoSomethingAction);
			NavigateMeCommand = new Command(NavigateMeAction);
			WelcomePerson = new Person() { Name = "John", Surname = "Doe" };
			SomePeople = new ObservableCollection<Person>()
			{
				new Person(){Name = "aaa", Surname = "bbb"},
				new Person(){Name = "ccc", Surname = "eee"},
				new Person(){Name = "ddd", Surname = "fff"},
			};
		}

		//public override void OnViewAppearing()
		//{
		//	base.OnViewAppearing();
		//}

		//public override void OnViewDisappearing()
		//{
		//	base.OnViewDisappearing();
		//}

		private void NavigateMeAction()
		{
			NavigationService.NavigateTo<SomePageViewModel>();
		}

		void TimerEventAction(TimerEvent evt)
		{
			CurrentTime = evt.Now.ToString();
			Device.BeginInvokeOnMainThread(() =>
			{
				SomePeople.Add(new Person() { Name = CurrentTime });
			});
		}

		private void DoSomethingAction()
		{
			WelcomePerson = new Person() { Name = "aaa", Surname = "bbb" };
			SomePeople.Add(new Person() { Name = "www", Surname = "xxx" });
		}

		public Person WelcomePerson
		{
			get { return _welcomePerson; }
			private set
			{
				_welcomePerson = value;
				NotifyPropertyChanged();
			}
		}

		public string CurrentTime
		{
			get { return _currentTime; }
			private set
			{
				_currentTime = value;
				NotifyPropertyChanged();
			}
		}

		public IList<Person> SomePeople { get; private set; }
		public ICommand DoSomethingCommand { get; private set; }
		public ICommand NavigateMeCommand { get; private set; }
	}
}